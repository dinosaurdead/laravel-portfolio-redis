<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use \Redis;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
//https://scotch.io/tutorials/getting-started-with-redis-in-php
    /*
        Redis::hmset('album:id:1', [
                'age' => 44,
                'country' => 'finland',
                'occupation' => 'software engineer',
                'reknown' => 'linux kernel',
        ]);
*/
        return view('home.index');
    }
}
