@extends('layouts.default')
@section('title', 'Home')
@section('content')
    <!-- Wrap all page content here -->
    <div id="wrap">
        @include('includes.slide')
        @include('includes.menu')

        <!-- Begin page content -->
        @include('includes.divider', ['sessionId' => 'section1'])
        @include('includes.leadtext')
        @include('includes.sectionbg', ['class' => 'bg-1'])
        @include('includes.divider', ['sessionId' => 'section2'])

        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
              <h1>Profile</h1>

              <hr>

              <p>
                The Firm has had a great deal of experience and is highly regarded for its expertise in the areas of design, construction administration,
                construction management, tight cost control and scheduling.
              </p>
              <p>
                We have been involved in a wide range of building projects, including college facilities, banks, schools,
                nursing homes, office buildings, churches, industrial buildings and major urban development projects.
              </p>
              <p>
                The various projects have included new construction, renovation and adaptive re-use as a way of providing new space for
                the various clients. Tessier Associates provides in-house programming, master planning, architectural design, construction documentation,
                project administration and interior design services. Sustainable design, as appropriate for each client,
                is incorporated in cost effective ways to benefit the long term value of the buildings created by the firm.
              </p>
              <p>
                Together with selected consultants, The Firm provides complete professional services including landscape architecture, structural engineering,
                electrical and mechanical engineering and site planning.
              </p>

              <hr>

              @include('includes.divider')

            </div><!--/col-->
        </div><!--/container-->

        @include('includes.divider')

        @include('includes.sectionbg', ['class' => 'bg-3', 'text' => 'Clients &amp; Partners'])
        @include('includes.divider', ['sessionId' => 'section3'])

        <div class="bg-4">
          <div class="container">
            <div class="row">
               <div class="col-sm-4 col-xs-6">

                <div class="panel panel-default">
                  <div class="panel-thumbnail"><a href="#" title="Renovations"><img src="//placehold.it/600x400/444/F8F8F8" class="img-responsive"></a></div>
                  <div class="panel-body">
                    <p>Renovations</p>
                    <p></p>

                  </div>
                </div><!--/panel-->
              </div><!--/col-->

              <div class="col-sm-4 col-xs-6">

                <div class="panel panel-default">
                  <div class="panel-thumbnail"><a href="#" title="Academic Institutions"><img src="//placehold.it/600x400/454545/FFF" class="img-responsive"></a></div>
                  <div class="panel-body">
                    <p>Academic Institutions</p>
                    <p></p>

                  </div>
                </div><!--/panel-->
              </div><!--/col-->

              <div class="col-sm-4 col-xs-6">

                <div class="panel panel-default">
                  <div class="panel-thumbnail"><a href="#" title="Interiors"><img src="//placehold.it/600x400/555/F2F2F2" class="img-responsive"></a></div>
                  <div class="panel-body">
                    <p>Interiors</p>
                    <p></p>

                  </div>
                </div><!--/panel-->

              </div><!--/col-->

              <div class="col-sm-4 col-xs-6">

                <div class="panel panel-default">
                  <div class="panel-thumbnail"><a href="#" title="New Construction"><img src="//placehold.it/600x400/555/FFF" class="img-responsive"></a></div>
                  <div class="panel-body">
                    <p>New Construction</p>
                    <p></p>

                  </div>
                </div><!--/panel-->

              </div><!--/col-->

              <div class="col-sm-4 col-xs-6">

                <div class="panel panel-default">
                  <div class="panel-thumbnail"><a href="#" title="Site Planning"><img src="//placehold.it/600x400/555/EEE" class="img-responsive"></a></div>
                  <div class="panel-body">
                    <p>Site Planning</p>
                    <p></p>

                  </div>
                </div><!--/panel-->

              </div><!--/col-->

              <div class="col-sm-4 col-xs-6">

                <div class="panel panel-default">
                  <div class="panel-thumbnail"><a href="#" title="Churches"><img src="//placehold.it/600x400/666/F4F4F4" class="img-responsive"></a></div>
                  <div class="panel-body">
                    <p>Churches</p>
                    <p></p>

                  </div>
                </div><!--/panel-->

              </div><!--/col-->

            </div><!--/row-->
          </div><!--/container-->
        </div>

        @include('includes.divider', ['sessionId' => 'section4'])

        <div class="row">
          <div class="col-md-8 col-md-offset-1">
          </div>
        </div>

        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
              <h1>Location</h1>
          </div>
          <div id="map-canvas"></div>
          <hr>
          <div class="col-sm-8"></div>
          <div class="col-sm-3 pull-right">
              <address>
                The Firm, Inc.<br>
                <span id="map-input">
                1500 Main Street<br>
                Springfield, MA 01115</span><br>
                P: (413) 700-5999
              </address>
              <address>
                <strong>Email Us</strong><br>
                <a href="mailto:#">first.last@example.com</a>
              </address>
          </div>
        </div><!--/row-->

        @include('includes.divider', ['sessionId' => 'section5'])

        <div class="row">
          <hr>
          <div class="col-sm-9 col-sm-offset-1">
              <div class="row form-group">
                <div class="col-md-12">
                <h1>Contact Us</h1>
                </div>
                <div class="col-xs-4">
                  <input type="text" class="form-control" id="firstName" name="name" placeholder="Your Name">
                </div>
                <div class="col-xs-6">
                  <input type="text" class="form-control" id="organization" name="organization" placeholder="Company or Organization">
                </div>
              </div>
              <div class="row form-group">
                  <div class="col-xs-5">
                  <input type="text" class="form-control" name="email" placeholder="Email">
                  </div>
                  <div class="col-xs-5">
                  <input type="text" class="form-control" name="phone" placeholder="Phone">
                  </div>
              </div>
              <div class="row form-group">
                  <div class="col-xs-10">
                    <textarea class="form-control" placeholder="Comments"></textarea>
                  </div>
              </div>
              <div class="row form-group">
                  <div class="col-xs-10">
                    <button class="btn btn-default pull-right">Contact Us</button>
                  </div>
              </div>
          </div>
        </div><!--/row-->

        <div class="container">
            <div class="col-sm-8 col-sm-offset-2 text-center">
              <ul class="list-inline center-block">
                <li><a href="http://facebook.com/bootply"><img src="http://www.bootply.com/assets/example/soc_fb.png"></a></li>
                <li><a href="http://twitter.com/bootply"><img src="http://www.bootply.com/assets/example/soc_tw.png"></a></li>
                <li><a href="http://google.com/+bootply"><img src="http://www.bootply.com/assets/example/soc_gplus.png"></a></li>
                <li><a href="http://pinterest.com/in1"><img src="http://www.bootply.com/assets/example/soc_pin.png"></a></li>
              </ul>
            </div><!--/col-->
        </div><!--/container-->

    </div><!--/wrap-->
    @include('includes.footer')
    @include('includes.backtotop')
    @include('includes.modal')
    <!-- script references -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&extension=.js&output=embed"></script>
    <script src="assets/js/scripts.js"></script>
@endsection