<section class="{{$class}}">
    <div class="col-sm-6 col-sm-offset-3 text-center">
        @if (!empty($text))
            <h2 style="padding:20px;background-color:rgba(5,5,5,.8)">{{$text}}</h2>
        @endif
    </div>
</section>