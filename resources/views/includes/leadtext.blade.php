<div class="container">
  <div class="col-sm-10 col-sm-offset-1">
    <div class="page-header text-center">
      <h1>Since 1923</h1>
    </div>

    <p class="lead text-center">
      The Firm has been providing Professional Design Services in the England area since 1923.
    </p>

    <hr>

    <div class="divider"></div>

  </div>
</div>