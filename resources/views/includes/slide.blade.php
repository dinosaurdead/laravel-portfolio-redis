<header class="masthead">

    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="item active">
          <img src="/assets/example/bg_5.jpg">
          <div class="container">
            <div class="carousel-caption">
              <h2>Architectural Design</h2>
              <p></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="/assets/example/bg_suburb.jpg">
          <div class="container">
            <div class="carousel-caption">
              <h2>Interior Design</h2>
              <p></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img src="/assets/example/bg_6.jpg">
          <div class="container">
            <div class="carousel-caption">
              <h2>Renovations &amp; Adaptive Re-use</h2>
              <p></p>
            </div>
          </div>
        </div>
      </div><!-- /.carousel-inner -->
      <div class="logo">The Firm</div>
      <!-- Controls -->
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
      </a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
      </a>
    </div>
    <!-- /.carousel -->

</header>