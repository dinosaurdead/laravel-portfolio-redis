<!DOCTYPE html>
<html lang="en">
    <head>
         <title>App Name - @yield('title')</title>
        @include('includes.head')
    </head>
    <body>
        @yield('content')
    </body>
</html>